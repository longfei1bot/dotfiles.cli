#!/usr/bin/env bash
trap '{ echo; urxvt-set -o 70; exit $?; }' INT EXIT
n=5
(($1)) && n=$1
while true; do
	for op in $(seq 0 $n 95) $(seq 100 -$n 5); do
		urxvt-set -o $op
		sleep 0.015
	done
done
