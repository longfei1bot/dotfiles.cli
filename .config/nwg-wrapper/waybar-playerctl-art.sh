#!/usr/bin/env bash
#shellcheck disable=2154,1091

. "$XDG_RUNTIME_DIR/waybar-playerctl.info"

# 'ln -s' to get a path without spaces
case $arturl in
	file://*) ln -sf "${arturl#file://}" "$XDG_RUNTIME_DIR/playerctl.nwg.art" ;;
	*) ln -sf /dev/null "$XDG_RUNTIME_DIR/playerctl.nwg.art" ;;
esac

printf '#img path=%s width=256 height=256\n<span size="xx-small">\n</span><b>%s</b>\n%s\n' \
	"$XDG_RUNTIME_DIR/playerctl.nwg.art" "$title" "$artist"
