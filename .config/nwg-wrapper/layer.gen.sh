#!/bin/sh
br='<span size="x-small">
</span>'
a='<span fgcolor="cyan" font_family="monospace">'
t='</span>'
cat << EOF
<span size="large"><b>Windows</b>
- ${a}mod+[hjkl]$t:  Change window focus
- ${a}mod+Shift+[hjkl]$t:  Move a container
- ${a}mod+Space$t: Toggle focus between tiled and floating windows
- ${a}mod+Shift+Space$t: Make a window floating/tiled
- ${a}mod+[az]$t:  Focus parent/child
- ${a}mod+r$t:  Enter resize mode
  - ${a}[hjkl]$t:  Resize
  - ${a}Shift+[hjkl]$t: Resize slightly
- ${a}mod+Shift+q$t: Kill a window
- ${a}mod+n$t: Split a container
- ${a}mod+w$t: Set a container's layout to tabbed
- ${a}mod+s$t: Set a container's layout to stacked
- ${a}mod+e$t: Set a container's layout to split h/v
- ${a}mod+f$t: Toggle fullscreen
- ${a}mod+Shift+g$t: Configure gaps
<b>Scratchpad</b>
- ${a}mod+c$t: Cycle scratchpad containers
- ${a}mod+v$t: Show popup terminal
<b>Workspaces</b>
- ${a}mod+[1-9,0]$t: Focus workspace 1-9, X
- ${a}mod+p$t: Focus media workspace
- ${a}mod+[,]$t: Focus games workspace
- ${a}mod+o$t: Focus document workspace
- ${a}mod+"["$t/$a"]"$t: Focus prev/next workspace
- ${a}mod+Shift+&lt;key&gt;$t: Move container to workspace &lt;key&gt;
<b>Launchers</b>
- ${a}mod+Return$t: Launch or focus a terminal in the terminal workspace
- ${a}mod+Shift+t$t: Launch a terminal in the current workspace
- ${a}mod+x$t:  Enter <b>focus/launch</b> mode
  - ${a}x$t:  Run wofi (desktop runner)
  - ${a}&lt;key&gt;$t: Attempt to focus the program before launching
  - ${a}Shift+&lt;key&gt;$t:  Launch the program regardless
  - Examples: ${a}w$t: Firefox, ${a}o$t: LibreOffice
<b>Notifications</b>
- ${a}mod+Delete$t: Dismiss a notification
- ${a}mod+Shift+Delete$t: Dismiss all notifications
- ${a}mod+Home$t: Restore a dismissed notification
- ${a}mod+Shift+Home$t: Invoke a notification's action
<b>System</b>
- ${a}mod+F1$t:  Display information about the running session
- ${a}mod+F3$t:  Lock the session
- ${a}mod+Shift+F3$t:  Exit the session
- ${a}mod+F4$t:  Enter system mode$t
EOF
