from __future__ import annotations
from typing import Callable, Any

import os
import pwd
import time
import logging

from newm.layout import Layout
from newm.helper import BacklightManager, WobRunner, PaCtl
from gi.repository import Playerctl, GLib

from pywm import (
    PYWM_MOD_LOGO,
    PYWM_MOD_ALT
)

logger = logging.getLogger(__name__)

background = {
    'path': os.environ.get('HOME','.') + '/Pictures/wallpaper/Games/Kaizo Trap/Kaizo_Trap_-_Poster_3.png',

    'anim': True
}

outputs = [
    { 'name': 'eDP-1' },
    { 'name': 'virt-1', 'pos_x': -1280, 'pos_y': 0, 'width': 1280, 'height': 720 }
]

pywm = {
    'encourage_csd': False,
    'focus_follows_mouse': False,
    'xkb_options': 'caps:escape_shifted_compose'
}

corner_radius = 8

focus = {
    'color': '#93a1a1',
    'animate_on_change': True,
    'anim_time': 0.1, # 6 frames
    'width': 3
}
view = {
    'corner_radius': 7,
    'padding': 9,
}

anim_time = 0.15 # 9 frames


wob_runner = WobRunner("wob -a bottom -M 100")
backlight_manager = BacklightManager(anim_time=1., bar_display=wob_runner)
kbdlight_manager = BacklightManager(args="--device='*::kbd_backlight'", anim_time=1., bar_display=wob_runner)
def synchronous_update() -> None:
    backlight_manager.update()
    kbdlight_manager.update()

pactl = PaCtl(0, wob_runner)


def on_startup():
    os.system("waybar &")
    os.system("playerctld daemon")

player = Playerctl.Player

def key_bindings(layout: Layout) -> list[tuple[str, Callable[[], Any]]]:
    return [
        ("L-h", lambda: layout.move(-1,  0)),
        ("L-j", lambda: layout.move( 0,  1)),
        ("L-k", lambda: layout.move( 0, -1)),
        ("L-l", lambda: layout.move( 1,  0)),
        ("L-Left",  lambda: layout.move(-1,  0)),
        ("L-Down",  lambda: layout.move( 0,  1)),
        ("L-Up",    lambda: layout.move( 0, -1)),
        ("L-Right", lambda: layout.move( 1,  0)),

        ("L-u", lambda: layout.basic_scale(1)),
        ("L-n", lambda: layout.basic_scale(-1)),
        ("L-t", lambda: layout.move_in_stack(1)),
        ("L-Tab", lambda: layout.move_in_stack(1)),
        ("L-S-Tab", lambda: layout.move_in_stack(-1)),

        ("L-H", lambda: layout.move_focused_view(-1, 0)),
        ("L-J", lambda: layout.move_focused_view(0, 1)),
        ("L-K", lambda: layout.move_focused_view(0, -1)),
        ("L-L", lambda: layout.move_focused_view(1, 0)),
        ("L-S-Left",  lambda: layout.move_focused_view(-1, 0)),
        ("L-S-Down",  lambda: layout.move_focused_view(0, 1)),
        ("L-S-Up",    lambda: layout.move_focused_view(0, -1)),
        ("L-S-Right", lambda: layout.move_focused_view(1, 0)),

        ("L-C-h", lambda: layout.resize_focused_view(-1, 0)),
        ("L-C-j", lambda: layout.resize_focused_view(0, 1)),
        ("L-C-k", lambda: layout.resize_focused_view(0, -1)),
        ("L-C-l", lambda: layout.resize_focused_view(1, 0)),
        ("L-C-Left",  lambda: layout.resize_focused_view(-1, 0)),
        ("L-C-Down",  lambda: layout.resize_focused_view(0, 1)),
        ("L-C-Up",    lambda: layout.resize_focused_view(0, -1)),
        ("L-C-Right", lambda: layout.resize_focused_view(1, 0)),

        ("L-x", lambda: os.system("wofi --show drun &")),
        ("L-Return", lambda: os.system("alacritty &")),
        ("L-q", lambda: layout.close_focused_view()),

        ("L-p", lambda: layout.ensure_locked(dim=True)),
        ("L-P", lambda: layout.terminate()),
        ("L-C", lambda: layout.update_config()),

        ("L-f", lambda: layout.toggle_fullscreen()),

        ("L-", lambda: layout.toggle_overview()),

        ("XF86MonBrightnessUp", lambda: backlight_manager.set(backlight_manager.get() + 0.1)),
        ("XF86MonBrightnessDown", lambda: backlight_manager.set(backlight_manager.get() - 0.1)),
        ("XF86KbdBrightnessUp", lambda: kbdlight_manager.set(kbdlight_manager.get() + 0.1)),
        ("XF86KbdBrightnessDown", lambda: kbdlight_manager.set(kbdlight_manager.get() - 0.1)),

        ("XF86AudioNext", lambda: os.system('playerctld shift')),
        ("XF86AudioPrev", lambda: os.system('playerctld unshift')),

        ("XF86AudioNext", player().next),
        ("XF86AudioPrev", player().previous),
        ("XF86AudioPlay", player().play_pause),
        ("XF86AudioStop", player().stop),
        ("XF86AudioRaiseVolume", lambda: pactl.volume_adj(5)),
        ("XF86AudioLowerVolume", lambda: pactl.volume_adj(-5)),
        ("XF86AudioMute", pactl.mute),
    ]

panels = {
    'lock': {
        'cmd': 'alacritty -e newm-panel-basic lock',
    },
    'launcher': {
        'cmd': 'alacritty -e newm-panel-basic launcher'
    },
}

energy = {
    'idle_callback': backlight_manager.callback
}
