#!/usr/bin/env zsh
fpath+=($ZDOTDIR/functions)
disable -a run-help which-command
autoload -Uz - $ZDOTDIR/functions/*(D:t) \
	zcalc zmv zargs run-help which-command add-zsh-hook zed

zsh_directory_name_functions+=(zdn_{vcs,mount} zdn_generic_alternate{,_complete})
zstyle -e :zdna:m dirs 'reply=(${(f)"$(findmnt -lno target)"})'
zstyle    :zdna:m match-by nofix prefix infix
zstyle -e :zdna:g dirs 'reply=($gitrepopath)'
zstyle    :zdna:g match-by sub nofix prefix infix

zle -N efn
(( ${plugins[(I)zsh-autosuggestions]} )) && bindkey '^ ' autosuggest-accept
sched +0 compdef __tmux-sessions tmax
sched +0 compdef _functions efn hifn
sched +0 compdef _aliases eal
sched +0 compdef _grc grc
sched +0 compdef _time_zone tz
sched +0 compdef _files d D
sched +0 compdef _mpv listento='mpv --shuffle --no-video'
sched +0 compdef _parameters printkv

(){ # lazy copy autoloaded functions when needed
	local a b
	for a b (
		sdu du
		'zcp zln' zmv
		D d
		foldl-reply foldl
	);{
		b='functions -c '$b' $0;$0 "$@"'
		for a ($=a);
			functions[$a]=$b
	}
}

# title / tabs
.set-title(){
	local d=("${(@s[/])${(%):-%~}}")
	if (($#d < 3)); then
		d=${(j:/:)d}
	else
		d=$d[1]/‥/$d[-1]
	fi
	if [[ -v TMUX ]]; then # just path
		printf '\e]0;%s\a' $d
		printf '\ek%s\a'   $d
	else
		printf '\e]0;%s\a' ${TERM:+${TERM[1]:u}${TERM[2,-1]:l} - }$d${SSH_CONNECTION+${(%)%n@%M}}
	fi
}
add-zsh-hook precmd .set-title

# Tabstops
if (($+terminfo)); then
	.on_winch(){
		# tabs
		local -a s=(${${terminfo[cuf]//(\%i|\%p1)}/\%d/$1}$terminfo[hts])
		repeat COLUMNS/$1 s+=($s[1])

		# final
		print -rn $terminfo[sc]${terminfo[hpa]//(\%i|\%p1|\%d)}$terminfo[tbc]${(j::)s}$terminfo[rc]
	}
	.on_winch 4
	trap '.on_winch 4' WINCH

elif (($+termcap)); then
	.on_winch(){
		# tabs
		local -a s
		repeat COLUMNS/$1 s+=(${termcap[RI]/\%p1\%d/$1}$termcap[st])

		# final
		print -rn $termcap[sc]${termcap[ch]//(\%i|\%p1|\%d)}$termcap[ct]${(j::)s}$termcap[rc]
	}
	.on_winch 4
	trap '.on_winch 4' WINCH
fi

λ lambda snippet(){
	eval "shift; $1"
}

bin2ascii(){
	printf %s ${(#)@/#/2#}
}

hyperlink(){
	if ((! $# || $# % 2)); then
		print -u2 "Usage: $0 [URL] [TEXT] [ [URL] [TEXT] ... ]"
		return 1
	fi
	typeset -ga reply=()
	printf -v reply '\e]8;;%s\a%s\e]8;;\a%s' "$@"
}

showargs() {
	printf '%d arguments\n' $#
	print -r - "[ ${(j<, >)${(q+)@}} ]"
}


rerun(){
	local pid=${1:?Missing PID}
	local -a reply env=(${(@ps[\0])"$(</proc/$pid/environ)"})
	zstat -A reply -L /proc/$pid/cwd
	pushd $reply[14]
	set -- "${(@ps[\0])"$(</proc/$pid/cmdline)"}"
	kill -- $pid
	command env -i - "${(@)env}" "$@"
	popd
}
compdef _kill rerun

1 onechar(){
	local free=({a..z} {A..Z})
	local i
	for i (aliases functions builtins commands){
		local l=(${(k)${(P)i}[(I)?]})
		free=(${free:|l})
		print "${(r:11:)i} ${(l:2:)#l}  ${(j< >)l}"
	}
	print "remaining   ${(l:2:)#free}  ${(j< >)free}"
}

with with0 wit0(){
	# Compare:
	# - with locate '*.ogg' do mpv
	# - mpv ${(f)"$(locate '*.ogg')"}

	# - with0 locate -0 '*.ogg' do mpv
	# - mpv ${(0)"$(locate -0 '*.ogg')"}

	# - with some-command
	# - reply=(${(f)"$(some-command)"})

	# Can't accomodate $( .. | .. ), but I guess
	# it can be composed with 'snippet' or 'eval'
	local -i do=$@[(i)do]
	local -a src=("${@[1,do - 1]//-do/do}")
	shift do
	if (($#)); then
		local -a reply=()
	else
		typeset -ga reply=()
	fi
	case $0 in
		*0) reply=(${(0)"$("${(@)src}")"}) ;;
		*) reply=(${(f)"$("${(@)src}")"})
	esac
	(($#)) && "$@" "${(@)reply}"
}


# math functions
autoload zmathfunc
zmathfunc  # loads min, max, sum
functions -M fact 1 1 zsh_math_func_fact
functions -M combi 2 2 zsh_math_func_combi
functions -M prod 0 -1 zsh_math_func_prod
functions -M avg 1 -1 zsh_math_func_avg
functions -M srandom 0 0 srandom
alias -g '$srandom=$((srandom()))'
functions -Ms numfmt 1 1 zsh_math_func_numfmt
functions -Ms print 1 1 zsh_math_func_print
functions -Ms roll 1 1 roll
