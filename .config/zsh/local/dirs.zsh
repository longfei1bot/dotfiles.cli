(){
	local n d
	for n d (
		-c $HOME/Documents/current
		-z $ZDOTDIR
		-x ${XDG_RUNTIME_DIR:-/run/user/$UID}
		-gvfs ${XDG_RUNTIME_DIR:-/run/user/1000}/gvfs
		-m /run/media/$USER
		_ /dev/null
		-nvim-git ${XDG_DATA_HOME:-$HOME/.local/share}/nvim/site/pack/git-plugins/start
		-pc ${XDG_CACHE_HOME:-$HOME/.cache}/pikaur/pkg
		-pb ${XDG_CACHE_HOME:-$HOME/.cache}/pikaur/build
		-pg ${XDG_DATA_HOME:-$HOME/.local/share}/pikaur/aur_repos
		-P /var/cache/pacman/pkg
		-ps /proc/self
		-tbb ${XDG_DATA_HOME:-$HOME/.local/share}/torbrowser/tbb/x86_64/tor-browser_en-US/Browser
	)
		[[ -e $d ]] && hash -d -- $n=$d
}
