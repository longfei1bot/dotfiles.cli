let g:solarized_contrast="high"
let g:solarized_visibility="high"
unlet g:solarized_termtrans
colorscheme solarized-truecolor
source /usr/share/nvim-gtk/runtime/nvim_gui_shim.vim
set number relativenumber
call rpcnotify(1, 'Gui', 'Option', 'Popupmenu', 0)
call rpcnotify(1, 'Gui', 'Option', 'Tabline', 0)
"if exists('g:GtkGuiLoaded')
"endif
