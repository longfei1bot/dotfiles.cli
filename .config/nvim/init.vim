"vim: set foldmethod=marker

" map leaders
let maplocalleader = ","

" lua conversion:
lua << EOF
-- map(<keymap>, <instring>, <outstring>)
local set = vim.opt
local map = function(key)
	local opts = {noremap = true}
	for i, v in pairs(key) do
		if type(i) == 'string' then opts[i] = v end
	end

	local buffer = opts.buffer
	opts.buffer  = nil
	if buffer then
		for kmap in key[1]:gmatch"." do
			vim.api.nvim_buf_set_keymap(0, kmap, key[2], key[3], opts)
		end
	else
		for kmap in key[1]:gmatch"." do
			vim.api.nvim_set_keymap(kmap, key[2], key[3], opts)
		end
	end
end

set.exrc     = true -- Per-dir .vimrc files
set.history  = 1000
set.ruler    = true -- show the cursor position all the time
set.showcmd  = true -- display incomplete commands
set.wildmenu = true -- display completion matches in a status line
set.wildmode = {"longest", "list", "full"} --  completion mode
set.omnifunc = "syntaxcomplete#Complete" -- ^
set.spell    = true
set.ttimeout = true -- time out for key codes

-- wait up to 20ms after Esc for special key
set.ttimeoutlen = 20
-- xterm title
set.title    = true
-- Alacritty terminfo broken apparently
if vim.env.TERM == "alacritty" then
	set.termguicolors = true
end
-- Show lines of context around the cursor at the top/bottom
set.scrolloff = 2
-- change working directory to file location
set.autochdir = true
-- buffers can be switched from without saving
set.hidden    = true

set.mouse = 'a'
set.ignorecase = true
set.smartcase  = true

set.list = true
set.listchars = {tab = '» ', extends = '›', precedes = '‹', nbsp = '·', trail = '·'}

set.conceallevel = 2
set.concealcursor = "c"
-- {{{ Indentation
set.autoindent  = true
set.shiftwidth  = 4
set.tabstop     = 4
set.softtabstop = 4
map {'v',   '<Tab>', '>gv'}
map {'v', '<S-Tab>', '<gv'}
map {'i', '<S-Tab>', '<C-D'}
-- }}}

-- Why is Y like this
map {'n', 'Y', 'y$'}

-- Cursor keys visual scroll
map {'i', "<up>", "<C-O>gk"}
map {'i', "<down>", "<C-O>gj"}
map {'nv', "<up>", "gk"}
map {'nv', "<down>", "gj"}

-- double escape to clear search highlight
map {'n', '<Esc><Esc>', '<Esc>:nohlsearch<CR><Esc>', silent = true }

-- date/time
map {'i', '<F2>', "<C-R>=strftime('%F')<C-M>", silent = true}
map {'i', '<F14>', "<C-R>=strftime('%FT%T')<C-M>", silent = true}

-- NeoPipe options
vim.g.npipe_type   = 'c'
vim.g.npipe_com    = 'zsh'
vim.g.npipe_append = 'bottom'
vim.g.npipe_sep    = { }

-- filetype specific {{{
-- {{{ vimtex
-- load vimtex
vim.g.tex_flavor='latex'
-- enable folding
vim.g.vimtex_fold_enabled=1
-- only fold sections
vim.g.vimtex_fold_types = {
	envs = {
		whitelist = {"frame"}
	}
}
vim.g.vimtex_view_method = 'zathura'
-- compiler options
vim.g.vimtex_compiler_latexmk = {
	build_dir = 'build'
}
-- }}}
-- {{{ json
-- local myag = vim.api.nvim_create_augroup("MyAuGroup", { clear = true })
-- vim.api.nvim_create_autocmd("FileType", {
-- 	pattern = "json",
-- 	command = vim.api.nvim_command('%!jq --tab . %'),
-- 	group = myag
-- })
-- }}}
-- }}}
-- Not sure if this helps...
set.switchbuf = { "usetab", "newtab" }

-- {{{ fzf selctions
map {'n', 'ZB', ':Buffers<C-R>'}
map {'n', 'ZF', ':Files<C-R>'}
map {'n', 'ZL', ':Lines<C-R>'}
-- vim.g.fzf_layout = { window: 'enew' }
-- vim.g.fzf_layout = { window: '-tabnew' }
vim.g.fzf_layout = { window = '10split enew' }
-- Customize fzf colors to match your color scheme
vim.g.fzf_colors = {
	fg      = { 'fg', 'Normal'},
	bg      = { 'bg', 'Normal'},
	hl      = { 'fg', 'Comment'},
	info    = { 'fg', 'PreProc'},
	border  = { 'fg', 'Ignore'},
	prompt  = { 'fg', 'Conditional'},
	pointer = { 'fg', 'Exception'},
	marker  = { 'fg', 'Keyword'},
	spinner = { 'fg', 'Label'},
	header  = { 'fg', 'Comment'}
}
vim.g.fzf_colors['fg+'] = { 'fg', 'CursorLine', 'CursorColumn', 'Normal'}
vim.g.fzf_colors['bg+'] = { 'bg', 'CursorLine', 'CursorColumn'}
vim.g.fzf_colors['hl+'] = { 'fg', 'Statement'}
-- }}}

-- Return at end of file
EOF

filetype plugin indent on

" {{{ search

if executable("rg")
	set grepprg=rg\ --vimgrep\ --no-heading
	set grepformat=%f:%l:%c:%m,%f:%l:%m
	command! -nargs=+ RG execute 'silent grep! <args>' | copen 15
else
	set grepprg=grep\ -nH\ $*
endif
" }}}

augroup vimStartup
	autocmd!
	" When editing a file, always jump to the last known cursor position.
	" Don't do it when the position is invalid, when inside an event handler
	" (happens when dropping a file on gvim) and for a commit message (it's
	" likely a different one than last time).
	autocmd BufReadPost *
	  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
	  \ |   exe "normal! g`\""
	  \ | endif
augroup END

" color {{{
augroup vimrc
	autocmd!
	set background=dark
	let g:solarized_termtrans=1
	colorscheme solarized-high
	" assume 256color, urxvt's terminfo is broken
	" solarized sets Normal fg text to blue for some reason,
	" fix that and set other highlight classes I want
	" TODO: I might fork solarized.vim to assume 256 and italics
	" and set these colors instead
	autocmd ColorScheme * highlight NonText ctermfg=0 cterm=underline gui=underline guifg=#073642
			\ | highlight SpecialKey ctermbg=0
			\ | highlight Comment ctermfg=242 cterm=italic
			\ | highlight Normal ctermfg=7
			\ | highlight Identifier ctermfg=NONE
			\ | highlight Function ctermfg=12
			\ | highlight Character ctermfg=33
			\ | highlight Number ctermfg=4 cterm=bold
			\ | highlight Boolean cterm=bold
			\ | highlight Search ctermbg=none ctermfg=none
			\ | highlight TabLineSel cterm=reverse,bold ctermbg=none ctermfg=blue
augroup END
" }}}

" diff current buffer with the file it was loaded from
command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
	  \ | wincmd p | diffthis

command -nargs=* G !git <args>

" Write and delete the current buffer
command -nargs=0 X w | bd

" haskell hdevtools integration
autocmd FileType haskell nnoremap <buffer> <F1> :HdevtoolsType<CR>
autocmd FileType haskell nnoremap <buffer> <silent> <F2> :HdevtoolsClear<CR>
" spaces recommended in Haskell
autocmd FileType haskell set expandtab
" git commit line length at 72, wrap on word
autocmd FileType gitcommit set tw=72 lbr

" read-only non-text filetypes
autocmd BufReadPre *.doc silent set ro
autocmd BufReadPost *.doc silent %!pandoc "%" -t markdown -o -

autocmd BufReadPre *.odt,*.odp silent set ro
autocmd BufReadPost *.odt,*.odp silent %!pandoc "%" -t markdown -o -

autocmd BufReadPre *.pdf silent set ro
autocmd BufReadPost *.pdf silent %!pdftotext -nopgbrk -layout -q -eol unix "%" -

autocmd BufReadPre *.rtf silent set ro
autocmd BufReadPost *.rtf silent %!pandoc "%" -t markdown -o -

autocmd BufNewFile,BufRead *.zsh-theme setlocal filetype=zsh

" Pandoc var
let g:pandoc#filetypes#pandoc_markdown = 1

" autoformat json
autocmd FileType json silent %!jq --tab . %

" fold manpages
autocmd FileType man set foldmethod=indent shiftwidth=3 foldnestmax=2
" }}}
" {{{ plugins in $XDG_DATA_HOME/nvim/site/pack/*/start
packloadall
silent! helptags ALL
" }}}

" vim: set foldmethod=marker:
