
# ==============================================
# Program Launchers
# ==============================================
set $fexec exec i3tool flaunch
# {{{ Screenshot / Record
# Printscreen (grim) whole-screen / selection / window
bindsym Print exec --no-startup-id grim-wrapper
bindsym Shift+Print exec --no-startup-id "grim-wrapper -s"
bindsym Control+Print exec --no-startup-id "grim-wrapper -w"

# Record screen: parec, ffmpeg
bindsym $mod+Print exec --no-startup-id sloprecord-dunst
bindsym $mod+Shift+Print exec --no-startup-id "sloprecord-dunst -s"
bindsym $mod+Control+Print exec --no-startup-id "sloprecord-dunst -w"

# Mumble Push-to-talk
bindsym --no-repeat $mod+F12 exec dbus-send --session --type=method_call \
	--dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.startTalking
bindsym --release $mod+F12 exec dbus-send --session --type=method_call \
	--dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.stopTalking
# }}}
# {{{ i3-input (for marking, etc) TODO: alternative
#bindsym $mod+i exec --no-startup-id i3-input
bindsym $mod+m exec swaymsg "[con_mark=$(swaymsg -t get_marks | jq -r '.[]' | wofi -d)]" focus
bindsym $mod+grave exec swaymsg -- mark --add --toggle "$(swaymsg -t get_marks | jq -r '.[]' | wofi -d)"
# }}}
# {{{ Standalone launchers
bindsym Shift+XF86Mail exec xdg-open $HOME
bindsym Shift+XF86HomePage exec firefox -P -no-remote hidden
bindsym $mod+$mod2+w exec firefox -P -no-remote hidden
bindsym $mod+Shift+t exec $terminal
# }}}
# {{{ Standalone focuslaunchers
bindsym XF86Mail $fexec thunderbird --class Thunderbird
bindsym XF86Search $fexec catfish
bindsym XF86Explorer $fexec catfish
bindsym XF86Calculator exec --no-startup-id exec echo
bindsym XF86Tools $fexec strawberry
bindsym XF86HomePage $fexec firefox --class firefox
# Terminal is the exception, I want more complex behavior
# (launch another if currently focused, and allow launching anywhere if necessary)
bindsym $mod+Return exec --no-startup-id exec i3tool slaunch "11:~$" $terminal --class $termclass

bindsym $mod+slash exec swaywindows focus
# }}}
# {{{ Focus/Launch mode
set $mode_launch Focus (_) / Launch (⇧ + _)
bindsym $mod+x mode "$mode_launch"
mode "$mode_launch" {
	bindsym Escape mode "default"
	
	# {{{ Launch, mode default
	bindsym Shift+a exec file-roller, mode "default"
	bindsym Shift+b exec blueman-manager, mode "default"
	bindsym Shift+c exec echo, mode "default"
	bindsym Shift+d exec echo, mode "default"
	bindsym Shift+e exec nvim-gtk, mode "default"
	bindsym Shift+f exec xdg-open ~, mode "default"
	bindsym Shift+g exec gimp, mode "default"
	bindsym Shift+h exec echo, mode "default"
	bindsym Shift+i exec echo, mode "default"
	bindsym Shift+j exec idea, mode "default"
	bindsym Shift+k exec kcmshell5 kcm_kdeconnect, mode "default"
	bindsym Shift+l exec libreoffice, mode "default"
	bindsym Shift+m exec thunderbird, mode "default"
	bindsym Shift+n exec echo, mode "default"
	bindsym Shift+o exec libreoffice, mode "default"
	bindsym Shift+p exec strawberry, mode "default"
	bindsym Shift+q mode "default"
	bindsym Shift+r exec echo, mode "default"
	bindsym Shift+s exec steam, mode "default"
	bindsym Shift+t exec torbrowser-launcher, mode "default"
	bindsym Shift+u exec echo, mode "default"
	bindsym Shift+v exec pavucontrol, mode "default"
	bindsym Shift+w exec firefox, mode "default"
	#bindsym Shift+x exec sgtk-grid -o 0.7, mode "default"
	bindsym Shift+x exec krunner, mode "default"
	bindsym Shift+y exec echo, mode "default"
	bindsym Shift+z exec echo, mode "default"
	bindsym Shift+slash exec catfish, mode "default"
	# }}}
	# {{{ Focus (exception dmenu/compton), mode "default"
	# The "i3focusLaunch" script will focus $2 if $2, else focus program name
	bindsym a $fexec file-roller, mode "default"
	bindsym b $fexec blueman-manager, mode "default"
	bindsym c echo, mode "default"
	bindsym d exec bemenu-run $bmopts, mode "default"
	bindsym e $fexec nvim-gtk --class Neovim, mode "default"
	bindsym f $fexec --class "(pcmanfm|thunar|dolphin)" xdg-open $HOME, mode "default"
	bindsym g $fexec gimp, mode "default"
	bindsym h $fexec echo, mode "default"
	bindsym i $fexec echo, mode "default"
	bindsym j $fexec --class=jetbrains-idea-ce idea, mode "default"
	bindsym k $fexec kcmshell5 kcm_kdeconnect, mode "default"
	bindsym l $fexec libreoffice, mode "default"
	bindsym m $fexec thunderbird --class Thunderbird, mode "default"
	bindsym n $fexec echo, mode "default"
	bindsym o $fexec libreoffice, mode "default"
	bindsym p $fexec strawberry, mode "default"
	bindsym q mode "default"
	bindsym r $fexec echo, mode "default"
	bindsym s $fexec steam, mode "default"
	bindsym t $fexec torbrowser-launcher, mode "default"
	bindsym u $fexec echo, mode "default"
	bindsym v $fexec pavucontrol, mode "default"
	bindsym w $fexec firefox --class firefox, mode "default"
	#bindsym x exec sgtk-grid -o 0.7, mode "default"
	bindsym x exec wofi --show drun, mode "default"
	bindsym $mod+x exec wofi --show drun, mode "default"
	bindsym y exec $fexec echo, mode "default"
	bindsym z exec $fexec echo, mode "default"
	bindsym slash exec $fexec catfish, mode "default"
	# }}}
	
	# bwutil menu
	bindsym       period         exec rbw-menu get     , mode "default"
	bindsym Shift+period         exec rbw-menu get type, mode "default"
	# get OTP codes
	bindsym Control+period       exec rbw-menu code     , mode "default"
	bindsym Control+Shift+period exec rbw-menu code type, mode "default"
}
# }}}
# {{{ Notifications
bindsym Control+$mod+p exec --no-startup-id sys-notif media
bindsym Control+$mod+s exec --no-startup-id sys-notif sensors
bindsym Control+$mod+d exec --no-startup-id sys-notif disk
bindsym Control+$mod+x exec --no-startup-id sys-notif cpu
bindsym Control+$mod+i exec --no-startup-id sys-notif ip
bindsym Control+$mod+u exec --no-startup-id sys-notif iusage
# }}}
# vim: set foldmethod=marker:
